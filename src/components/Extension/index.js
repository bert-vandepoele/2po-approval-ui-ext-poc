// @flow

import React, { Component } from "react";
import { Note } from "@contentful/forma-36-react-components";
import "@contentful/forma-36-react-components/dist/styles.css";
import "@contentful/forma-36-fcss/dist/styles.css";

/**
 * Extension
 */
export default class Extension extends Component<*> {
    /**
     * Component Mount => Setup Contentful UI extension
     */
    componentDidMount() {
        const { sdk } = this.props;
        this.props.sdk.window.startAutoResizer();
        console.log(sdk);
        console.log(sdk.user.spaceMembership.roles);
    }

    /**
     * Render => Contentful UI Extension
     */
    render() {
        return (
            <Note className="f36-margin-bottom-xs" noteType="primary">
                Blueprint for Contentful UI Extensions currently under
                construction
            </Note>
        );
    }
}
